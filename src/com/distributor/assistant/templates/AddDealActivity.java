package com.distributor.assistant.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Customer;
import com.distributor.assistant.database.Deal;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kavi on 9/19/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class AddDealActivity extends Activity {

    private EditText dealNameEditText;
    private Spinner customerSpinner;
    private DatePicker datePicker;
    private Button addDealButton;

    final Context context = this;
    private AlertDialog messageBalloonAlertDialog;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    private long customerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_deal);

        setUpView();
    }

    private void setUpView() {

        Bundle extras = getIntent().getExtras();
        customerId = extras.getLong("CUSTOMER_ID");

        dealNameEditText = (EditText) findViewById(R.id.dealNameEditText);
        customerSpinner = (Spinner) findViewById(R.id.customersSpinner);
        datePicker = (DatePicker) findViewById(R.id.datePicker);
        addDealButton = (Button) findViewById(R.id.addDealButton);

        List<Customer> customerList = localDatabaseSQLiteOpenHelper.getAllCustomers();
        List<String> customerNames = new ArrayList<String>();
        int count = 0;
        int position = 0;
        for (Customer customer : customerList) {
            customerNames.add(customer.getCustomerName() + " - " + customer.getCustomerId());
            if(customerId == customer.getCustomerId()) {
                position = count;
            }
            count++;
        }

        ArrayAdapter<String> customerDataAdapter = new ArrayAdapter<String>(this, R.layout.customer_row_view, customerNames);
        customerDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        customerSpinner.setAdapter(customerDataAdapter);
        customerSpinner.setSelection(position);

        datePicker.setEnabled(false);

        addDealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dealName = dealNameEditText.getText().toString();
                String customer = customerSpinner.getSelectedItem().toString();
                String cusContent[] = customer.split("-");
                String cusId = cusContent[1].replace(" ", "");

                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");

                Deal deal = new Deal();
                deal.setDealName(dealName);
                deal.setCustomerId(Integer.parseInt(cusId));
                deal.setDealDate(sdf.format(date));

                final long dealId = localDatabaseSQLiteOpenHelper.saveDeal(deal);

                if(dealId != 0) {
                    //TODO need to handle the user press back button
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.success)
                            .setMessage("New deal added successfully. Add products to your deal " + dealId)
                            .setPositiveButton("Add products", new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent addNewProductDealIntent = new Intent(AddDealActivity.this, AddProductDealActivity.class);
                                    addNewProductDealIntent.putExtra("ROUTE", "AddDealActivity");
                                    addNewProductDealIntent.putExtra("DEAL_ID", dealId);
                                    startActivity(addNewProductDealIntent);
                                    finish();
                                }
                            })
                            .setNeutralButton("Deals List", new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent dealsIntent = new Intent(AddDealActivity.this, DealsListActivity.class);
                                    startActivity(dealsIntent);
                                    finish();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                }
            }
        });
    }
}
