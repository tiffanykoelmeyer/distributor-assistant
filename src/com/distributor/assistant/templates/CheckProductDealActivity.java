package com.distributor.assistant.templates;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Customer;
import com.distributor.assistant.database.Deal;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;
import com.distributor.assistant.database.ProductDeal;
import com.distributor.assistant.service.SendEmail;

import java.util.List;

/**
 * Created by kavi on 9/28/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class CheckProductDealActivity extends Activity {

    private TextView dealIdTextView;
    private TextView productNameTextView;
    private TextView weightTextView;
    private TextView unitPriceTextView;
    private TextView qtyTextView;
    private TextView discountTextView;
    private TextView totalTextView;

    private Button editButton;
    private Button finishButton;
    private Button nextButton;

    private long dealId;
    private String productName;
    private String weight;
    private String unitPrice;
    private String qty;
    private String discount;
    private Double totalPrice;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_procut_deal_layout);

        setUpView();
    }

    private void setUpView() {
        dealIdTextView = (TextView) findViewById(R.id.checkDealIdTextView);
        productNameTextView = (TextView) findViewById(R.id.productNameTextView);
        weightTextView = (TextView) findViewById(R.id.weightTextView);
        unitPriceTextView = (TextView) findViewById(R.id.unitPriceTextView);
        qtyTextView = (TextView) findViewById(R.id.qtyTextView);
        discountTextView = (TextView) findViewById(R.id.discountTextView);
        totalTextView = (TextView) findViewById(R.id.totalTextView);

        editButton = (Button) findViewById(R.id.backToEditButton);
        finishButton = (Button) findViewById(R.id.finishDealButton);
        nextButton = (Button) findViewById(R.id.nextProductAddButton);

        Bundle extras = getIntent().getExtras();
        dealId = extras.getLong("DEAL_ID");
        productName = extras.getString("PRODUCT_NAME");
        weight = extras.getString("PRODUCT_WEIGHT");
        unitPrice = extras.getString("UNIT_PRICE");
        qty = extras.getString("QTY");
        discount = extras.getString("DISCOUNT");
        totalPrice = extras.getDouble("TOTAL");

        dealIdTextView.setText(String.valueOf(dealId));
        productNameTextView.setText(productName);
        weightTextView.setText(weight);
        unitPriceTextView.setText(unitPrice);
        qtyTextView.setText(qty);
        discountTextView.setText(discount);
        totalTextView.setText(String.valueOf(totalPrice));

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addProductDealIntent = new Intent(CheckProductDealActivity.this, AddProductDealActivity.class);
                addProductDealIntent.putExtra("ROUTE", "checkProductDealActivity");
                addProductDealIntent.putExtra("DEAL_ID", dealId);
                addProductDealIntent.putExtra("PRODUCT_NAME", productName);
                addProductDealIntent.putExtra("PRODUCT_WEIGHT", weight);
                addProductDealIntent.putExtra("UNIT_PRICE", unitPrice);
                addProductDealIntent.putExtra("QTY", qty);
                addProductDealIntent.putExtra("DISCOUNT", discount);

                startActivity(addProductDealIntent);
                finish();
            }
        });

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProductDeal productDeal = new ProductDeal();
                productDeal.setDealId(dealId);
                productDeal.setProductName(productName);
                productDeal.setWeight(Double.parseDouble(weight));
                productDeal.setUnitPrice(Double.parseDouble(unitPrice));
                productDeal.setQuantity(Double.parseDouble(qty));
                productDeal.setDiscount(Double.parseDouble(discount));
                productDeal.setTotalAmount(totalPrice);

                try {
                    localDatabaseSQLiteOpenHelper.saveProductDeal(productDeal);
                } catch (SQLiteException ex) {
                    throw  ex;
                }

                Deal getDeal = localDatabaseSQLiteOpenHelper.getDealFromDealId(dealId);
                List<ProductDeal> productDealsList = localDatabaseSQLiteOpenHelper.getProductDealsFromDealId(dealId);

                Double totalDealAmount = 0.0;
                for (ProductDeal getProductDeal : productDealsList) {
                    totalDealAmount = totalDealAmount + getProductDeal.getTotalAmount();
                }

                localDatabaseSQLiteOpenHelper.updateDealTotalAndDiscountAmount(0.0, totalDealAmount, dealId);

                Intent dealSummeryIntent = new Intent(CheckProductDealActivity.this, DealSummeryActivity.class);
                dealSummeryIntent.putExtra("DEAL_ID", dealId);
                dealSummeryIntent.putExtra("ROUTE",1);
                startActivity(dealSummeryIntent);
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProductDeal productDeal = new ProductDeal();
                productDeal.setDealId(dealId);
                productDeal.setProductName(productName);
                productDeal.setWeight(Double.parseDouble(weight));
                productDeal.setUnitPrice(Double.parseDouble(unitPrice));
                productDeal.setQuantity(Double.parseDouble(qty));
                productDeal.setDiscount(Double.parseDouble(discount));
                productDeal.setTotalAmount(totalPrice);

                try {
                    localDatabaseSQLiteOpenHelper.saveProductDeal(productDeal);
                } catch (SQLiteException ex) {
                    throw  ex;
                }

                Intent addNewProductDealIntent = new Intent(CheckProductDealActivity.this, AddProductDealActivity.class);
                addNewProductDealIntent.putExtra("ROUTE", "AddDealActivity");
                addNewProductDealIntent.putExtra("DEAL_ID", dealId);
                startActivity(addNewProductDealIntent);
                finish();
            }
        });
    }
}
