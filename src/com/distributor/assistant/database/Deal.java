package com.distributor.assistant.database;

/**
 * Created by kavi on 9/19/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class Deal {

    private int dealId;
    private String dealName;
    private int customerId;
    private String dealDate;
    private Double dealDiscount;
    private Double dealTotalAmount;

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getDealDate() {
        return dealDate;
    }

    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }

    public Double getDealDiscount() {
        return dealDiscount;
    }

    public void setDealDiscount(Double dealDiscount) {
        this.dealDiscount = dealDiscount;
    }

    public Double getDealTotalAmount() {
        return dealTotalAmount;
    }

    public void setDealTotalAmount(Double dealTotalAmount) {
        this.dealTotalAmount = dealTotalAmount;
    }
}
