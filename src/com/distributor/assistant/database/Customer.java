package com.distributor.assistant.database;

/**
 * Created by kavi on 9/17/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class Customer {

    private int customerId;
    private String customerName;
    private String customerAddress;
    private String customerContactNo;
    private String addedDate;
    private String dateForNextVisit;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerContactNo() {
        return customerContactNo;
    }

    public void setCustomerContactNo(String customerContactNo) {
        this.customerContactNo = customerContactNo;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getDateForNextVisit() {
        return dateForNextVisit;
    }

    public void setDateForNextVisit(String dateForNextVisit) {
        this.dateForNextVisit = dateForNextVisit;
    }
}
